//================================================================================================================//
// Class: QueueRecordDMLTest
// Developer: Vertiba.Sapient
// This class provides unit test coverage for the class 'QueueRecordDML'.
//
// TEST SETUP METHODS:
//		- createTestData()
//
// TEST METHODS:
//		- 
//		- 
//
//================================================================================================================//
@isTest
private class QueueRecordDMLTest 
{
	
	private static list<Account> testAccounts;

	//============================================ -- TEST SETUP METHODS -- ======================================//
	
	//========================================================================================
	// Test Setup Method: createTestData()
	// Create test data for the test methods to use. Called once before any test methods run. 
    // Data is reset to to this state between each test method.
	@testSetup static void createTestData() 
	{
		ApexLoggingUtils.addLogMessage('************************ -- START CREATING TEST DATA -- ************************');
		
		testAccounts = new list<Account>();
		testAccounts.add(new Account(Name='Test Acct 1',Phone='111-867-5309'));
		testAccounts.add(new Account(Name='Test Acct 2',Phone='222-867-5309'));
		insert testAccounts;

		ApexLoggingUtils.addLogMessage('************************ -- DONE CREATING TEST DATA -- ************************');
	}

	//============================================ -- TEST METHODS -- ============================================//
	
	//========================================================================================
    // Method: testRecordInsert()
    @isTest static void testRecordInsert()
    {
    	Id jobId;
    	testAccounts = new list<Account>();
		testAccounts.add(new Account(Name='Test Acct 3',Phone='333-867-5309'));
		testAccounts.add(new Account(Name='Test Acct 4',Phone='444-867-5309'));
		
		// -- Start the test -- //
        Test.startTest();

        QueueRecordDML insertJob1 = new QueueRecordDML('Insert',testAccounts);
        jobID = System.enqueueJob(insertJob1);

        // -- Stop the test -- //
        Test.stopTest();

        testAccounts = [SELECT Id,Name,Phone FROM Account];
        System.assert(testAccounts.size() == 4,'Expected a total of 4 Test Accounts. Actual: ' + testAccounts.size());

    }

    //========================================================================================
    // Method: testRecordUpdate()
    @isTest static void testRecordUpdate()
    {
    	Id jobId;
    	testAccounts = [SELECT Id,Name,Phone FROM Account ORDER BY Name ASC];
		testAccounts[0].Phone='111-111-1111';
		testAccounts[1].Phone='222-222-2222';
		
		// -- Start the test -- //
        Test.startTest();

        QueueRecordDML updateJob1 = new QueueRecordDML('Update',testAccounts);
        jobID = System.enqueueJob(updateJob1);

        // -- Stop the test -- //
        Test.stopTest();

        testAccounts = [SELECT Id,Name,Phone FROM Account ORDER BY Name ASC];
        System.assert(testAccounts[0].Phone == '111-111-1111','Expected a phone number of: 111-111-1111. Actual: ' + testAccounts[0].Phone);
        System.assert(testAccounts[1].Phone == '222-222-2222','Expected a phone number of: 222-222-2222. Actual: ' + testAccounts[1].Phone);
    }

    //========================================================================================
    // Method: testRecordDelete()
    @isTest static void testRecordDelete()
    {
    	Id jobId;
    	testAccounts = [SELECT Id,Name,Phone FROM Account ORDER BY Name ASC];
		
		// -- Start the test -- //
        Test.startTest();

        QueueRecordDML deleteJob1 = new QueueRecordDML('delete',testAccounts);
        jobID = System.enqueueJob(deleteJob1);

        // -- Stop the test -- //
        Test.stopTest();

        testAccounts = [SELECT Id,Name,Phone FROM Account ORDER BY Name ASC];
        System.assert((testAccounts == null || testAccounts.isEmpty()),'Expected No Accounts to exist. Actual: ' + testAccounts);
    }

    //========================================================================================
    // Method: testRecordUndelete()
    @isTest static void testRecordUndelete()
    {
    	Id jobId;
    	testAccounts = [SELECT Id,Name,Phone FROM Account ORDER BY Name ASC];
		
		// -- Start the test -- //
        Test.startTest();

        // Request Deletion
        QueueRecordDML deleteJob = new QueueRecordDML('delete',testAccounts);
        jobID = System.enqueueJob(deleteJob);

        // Insert 20 No-Operation Test Jobs (to ensure delete and undelete aren't processing together)
        Test.enqueueBatchJobs(55); 

        // Insert 
        QueueRecordDML undeleteJob = new QueueRecordDML('Undelete',testAccounts);
        jobID = System.enqueueJob(undeleteJob);

        // -- Stop the test -- //
        Test.stopTest();

        testAccounts = [SELECT Id,Name,Phone FROM Account ORDER BY Name ASC];
        System.assert(testAccounts.size() == 2,'Expected 2 Accounts to exist. Actual: ' + testAccounts.size());
    }

    //========================================================================================
    // Method: testBulkRecordInsert()
    @isTest static void testBulkRecordInsert()
    {
    	Id jobId;
    	Integer numBulkRecords = 50;
    	testAccounts = new list<Account>();
    	for (Integer i=0; i < numBulkRecords; i++)
    	{
    		String thisName = 'Test Bulk Acct ' + String.valueOf(i);
    		testAccounts.add(new Account(Name=thisName,Phone='999-999-9999'));
    	}
		
		// -- Start the test -- //
        Test.startTest();

        QueueRecordDML insertJob1 = new QueueRecordDML('Insert',testAccounts);
        jobID = System.enqueueJob(insertJob1);

        // -- Stop the test -- //
        Test.stopTest();

        testAccounts = [SELECT Id,Name,Phone FROM Account WHERE NAME LIKE 'Test Bulk Acct%' ORDER BY Name ASC];
        System.assert(testAccounts.size() > 0,'Expected at least 10 Bulk Accounts to be created. Actual: ' + testAccounts.size());

    }

    //========================================================================================
    // Method: testBadCalls()
    @isTest static void testBadCalls()
    {
    	Id jobId;
    	testAccounts = [SELECT Id,Name,Phone FROM Account];

    	// -- Start the test -- //
        Test.startTest();

        // Send an invalid dml command
        QueueRecordDML badJob1 = new QueueRecordDML('Destroy',testAccounts);
        jobID = System.enqueueJob(badJob1);

        // Send an empty list of objects
        QueueRecordDML badJob2 = new QueueRecordDML('insert',new list<Account>());
        jobID = System.enqueueJob(badJob2);

        // Call without any arguments
        QueueRecordDML badJob3 = new QueueRecordDML();
        jobID = System.enqueueJob(badJob3);

        // -- Stop the test -- //
        Test.stopTest();
    }
	
}