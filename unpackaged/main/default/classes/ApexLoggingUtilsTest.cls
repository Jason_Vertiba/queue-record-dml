//================================================================================================================//
// Class: ApexLoggingUtilsTest 
// Developer: Vertiba.Sapient
// This class provides unit test coverage for the class 'ApexLoggingUtils'.
//
// TEST SETUP METHODS:
//		- createTestData()
//
// TEST METHODS:
//		- testAddDebugLogMessage()
//		- testAddWarnLogMessage()
//		- testAddErrorLogMessage()
//
//================================================================================================================//
@isTest
private class ApexLoggingUtilsTest 
{
	
	//============================================ -- TEST SETUP METHODS -- ======================================//

	//========================================================================================
	// Test Setup Method: createTestData()
	// Create test data for the test methods to use. Called once before any test methods run. 
    // Data is reset to to this state between each test method.
	@testSetup static void createTestData() 
	{

	}
	
	//=============================================== -- TEST METHODS -- ===================================================//

	//========================================================================================
	// Method: testAddDebugLogMessage() 
	@isTest static void testAddDebugLogMessage() 
	{
		Test.startTest();
		ApexLoggingUtils.addLogMessage('Running unit test: testAddDebugLogMessage()'); 
		Test.stopTest();
	}
	
	//========================================================================================
	// Method: testAddWarnLogMessage() 
	@isTest static void testAddWarnLogMessage() 
	{
		Test.startTest();
		ApexLoggingUtils.addLogMessage(LoggingLevel.WARN,'Running unit test: testAddWarnLogMessage()'); 
		Test.stopTest();
	}

	//========================================================================================
	// Method: testAddErrorLogMessage() 
	@isTest static void testAddErrorLogMessage() 
	{
		Test.startTest();
		ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Running unit test: testAddErrorLogMessage()'); 
		Test.stopTest();
	}
}