//==============================================================================================================//
// Class: QueueRecordDML
// Developer: Vertiba.Sapient
// A Queueable Apex class for performing DML operations on Salesforce records.
//	- All records must be of the same Object Type
//	- Constructor takes a DML Operation String: insert,update,delete,undelete
// 	- Constructor takes a list of sObjects.
//==============================================================================================================//
public class QueueRecordDML implements Queueable 
{
	// -- Processing Variables -- //
    private Integer totalDMLRowsAllowed {get;set;}									// Track actual system limit remaining in current context. Access this via helper methods.
    private Boolean hasArgumentError {get;set;}										// Whether or not we've received valid arguments
    //private Boolean hasProcessingError {get;set;}									// Whether or not we've encountered an error during processing
    //private list<Exception> exceptionList {get;set;}								// List of Exceptions caught during processing.
    private Boolean needSubProcess {get;set;}										// Whether or not we need to generate a sub-process for additional processing  
    private String dmlOperationArg {get;set;}										// The requested DML Operation to perform
    private list<sObject> sObjectListArg {get;set;}									// The list of Salesforce Objects upon which to perform the requested DML Operation
    private list<sObject> sObjectSubSet {get;set;}									// A subset of Salesforce Objects upon which to perform the requested DML Operation
    private FINAL set<String> dmlOperations = new set<String>{'insert','update','delete','undelete'};

	//================================= -- CONSTRUCTORS -- =========================================//

	//=================================================================================
	// Constructor: QueueRecordDML()
	public QueueRecordDML()
	{
		ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'The QueueRecordDML constructor requires arguments.');
		hasArgumentError = true;
	}

	//=================================================================================
	// Constructor: QueueRecordDML (ShareRequestWrapper requestWrapper)
	public QueueRecordDML (String dmlOperation, list<sObject> sObjectList)
	{
		// -- Initialize class variables -- //
		needSubProcess = false;
		hasArgumentError = false;
		// When testing, set the total allowed rows to 10 (easier to test)
		totalDMLRowsAllowed = (Test.isRunningTest()) ? 10 : (Limits.getLimitDMLRows() - Limits.getDMLRows());
		sObjectListArg = new list<sObject>();
		sObjectSubSet = new list<sObject>();

		// Check if received valid wrapper
		if (String.isEmpty(dmlOperation) || !dmlOperations.contains(dmlOperation.trim().toLowerCase()) 
			|| sObjectList == null || sObjectList.isEmpty())
		{
			ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Invalid Arguments!! - dmlOperation: ' + dmlOperation + ' sObjectList: ' + sObjectList);
			hasArgumentError = true;
		}
		else
		{
			dmlOperationArg = dmlOperation.trim().toLowerCase();
			sObjectListArg = sObjectList;
		}
	}

	//================================= -- PUBLIC METHODS -- ======================================//

	//=====================================================================================
	// Method: execute() 
	// Execute the logic for this batch process.
	public void execute(QueueableContext context) 
	{
        if (hasArgumentError)
        {
        	ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Invalid Arguments were receieved. Exiting processing...');
        	return;
        }

        // -- Check the size of the list and get subset if necessary -- //
        if (sObjectListArg.size() > totalDMLRowsAllowed)
        {
        	needSubProcess = true;
        	for (Integer i=0; i<totalDMLRowsAllowed; i++)
        		{sObjectSubSet.add(sObjectListArg[i]);}
        }
        else
        	{sObjectSubSet.addAll(sObjectListArg);}

        // -- Cast the List of Records -- //
        list<sObject> castObjRecords;
        Schema.SObjectType objType;
        String listType;
		String objectAPIName = SchemaUtils.getObjectTypeNameFromObject(sObjectSubSet[0]); // <= Use first record to determine type
		if (objectAPIName != null)
		{
			objType = SchemaUtils.getObjectType(objectAPIName);
			listType = 'list<' + objType + '>';
			castObjRecords = (list<sObject>)Type.forName(listType).newInstance();
			castObjRecords.addAll(sObjectSubSet);
		}
		else
			{ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Unable to determine Object Type for record: ' + sObjectSubSet[0]);}

		// -- Perform the DML -- //
		if (castObjRecords != null && !castObjRecords.isEmpty())
		{
	        if (dmlOperationArg == 'insert')
	        {
	        	try
	        		{insert castObjRecords;}
	        	catch (Exception e)
	        	{
	        		ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Error Inserting records: ' + e);
	        		//hasProcessingError = true;
	        	}
	        }
	        else if (dmlOperationArg == 'update')
	        {
	        	try
	        		{update castObjRecords;}
	        	catch (Exception e)
	        	{
	        		ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Error Updating records: ' + e);
	        		//hasProcessingError = true;
	        	}
	        }
	        else if (dmlOperationArg == 'delete')
	        {
	        	try
	        		{delete castObjRecords;}
	        	catch (Exception e)
	        	{
	        		ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Error Deleting records: ' + e);
	        		//hasProcessingError = true;
	        	}
	        }
	        else if (dmlOperationArg == 'undelete')
	        {
	        	try
	        		{undelete castObjRecords;}
	        	catch (Exception e)
	        	{
	        		ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'Error Undeleting records: ' + e);
	        		//hasProcessingError = true;
	        	}
	        }
		}
		else
			{ApexLoggingUtils.addLogMessage(LoggingLevel.ERROR,'No records survived being cast.');}

        // -- Launch SubProcess if Needed -- //
        if (needSubProcess)
        {
        	Id jobID;
        	// Remove processed records
			set<sObject> sObjArgSet = new set<sObject>(sObjectListArg);
			set<sObject> sObjProcessedSet = new set<sObject>(sObjectSubSet);
			sObjArgSet.removeAll(sObjProcessedSet);
			list<sObject> newObjSubList = new list<sObject>(sObjArgSet);
			// Add new process to queue
			QueueRecordDML queueDML = new QueueRecordDML(dmlOperationArg,newObjSubList);
            // If this is a test, and we're already being called by a batch or queueable process, don't attempt to create another asynchronous process
            Boolean safeToLaunchAsynch = (Test.isRunningTest() && (System.isBatch() || System.isQueueable())) ? false : true;
            // Put the batch job into the queue
            if (safeToLaunchAsynch)
            	{jobID = System.enqueueJob(queueDML);}
            ApexLoggingUtils.addLogMessage('Created a Queued Apex Job sub-process: ' + jobId + ' to perform DML operation: ' + dmlOperationArg + ' to: ' + newObjSubList.size() + ' records.');
        }
	}
}