//==============================================================================================================//
// Class 		: ApexLoggingUtils.cls
// Developer 	: Vertiba.Sapient
// Created   	: 1/9/2018
// Objective 	: This class provides static utility methods for Apex logging. This ensures consistent logging output.
// Test Class 	: ApexLoggingUtilsTest.cls 
// Notes		: All methods are static and run WITHOUT SHARING.
//
// PUBLIC METHODS:
//		- addLogMessage(String msg)		
//				Adds log message with a LoggingLevel of debug.
//		- addLogMessage(LoggingLevel logLevel, String msg) 
//				Adds log message at the specified Logging Level. Errors generate a Corporate Log record.
//				Logging Level options are: LoggingLevel.DEBUG, LoggingLevel.WARN, LoggingLevel.ERROR
// PRIVATE METHODS:
//		- addLogMessage(LoggingLevel logLevel, String sendingClassName, String sendingMethodName, String lineNumberInfo, String msg)
//			 	Given a requested log level, name of the Class sending the request, the name of the Method sending 
//			  	the request, and a message, generates a debug log entry and calls any custom logging logic.
//		- getClassName() => String
// 				Parses a stack trace string and returns the name of the Class that was executing.
//		- getMethodName() => String
// 				Parses a stack trace string and returns the name of the Method that was executing.
//		- getLineColNumberString (String traceLine) => String
// 				Parses a stack trace string and returns the Line Number and Column Number where 
// 				the error occurred, as a String.
//
//==============================================================================================================//
public without sharing class ApexLoggingUtils 
{
	public static Boolean useClassDebugging {get;set;}
	public static Boolean writeToExceptionLogOnError {get;set;}

	//=================================================================================
	// Static Initializer
	static 
	{	
		useClassDebugging = false;			// <= Default debugging to false. Set from calling method using: ApexLoggingUtils.useClassDebugging = true;
		writeToExceptionLogOnError = true; 	// <= By default, generate Exception Log records for errors 
	}

	//================================= -- PUBLIC METHODS -- ==================================================//

	//=================================================================================
	// Method: addLogMessage(String msg)
	// Given a message, generates a debug log entry of level "debug".
	public static void addLogMessage(String msg) 
	{
		// Get the trace string for the class/method that called called this method (this is the fast way)
		String trace = new DmlException().getStackTraceString().substringAfter('\n').substringBefore('\n'); 
		if (useClassDebugging){System.debug('ApexLoggingUtils.addLogMessage(String msg) - Entering Method. Retrieved Stack Trace: ' + trace);}
		// Call the private method that handles the real work
    	addLogMessage(LoggingLevel.DEBUG,getClassName(trace),getMethodName(trace),getLineColNumberString(trace),msg);
    	if (useClassDebugging){System.debug('ApexLoggingUtils.addLogMessage(String msg) - Log Message Added. Exiting method...');}
   	}

	//=================================================================================
	// Method: addLogMessage(LoggingLevel logLevel,String msg)
	// Given a log level and a message, generates a debug log entry and handles any 
	// custom logging logic.
	public static void addLogMessage(LoggingLevel logLevel,String msg) 
	{
		// Get the trace string for the class/method that called called this method (this is the fast way)
		String trace = new DmlException().getStackTraceString().substringAfter('\n').substringBefore('\n'); 
		if (useClassDebugging){System.debug('ApexLoggingUtils.addLogMessage(LoggingLevel logLevel,String msg) - Entering Method. Retrieved Stack Trace: ' + trace);}
		// Call the private method that handles the real work
    	addLogMessage(logLevel,getClassName(trace),getMethodName(trace),getLineColNumberString(trace),msg);
    	if (useClassDebugging){System.debug('ApexLoggingUtils.addLogMessage(LoggingLevel logLevel,String msg) - Log Message Added. Exiting method...');}
   	}

	//================================= -- PRIVATE METHODS -- ==================================================//

	//=================================================================================
	// Method: addLogMessage()
	// Given the name of the method calling it, the requested log level, and a message,
	// generates a debug log entry and calls any custom logging.
	private static void addLogMessage(LoggingLevel logLevel,String sendingClassName,String sendingMethodName,String lineNumberInfo,String msg) 
	{
		String logMsg = sendingClassName + '.' + sendingMethodName + '() - ' + lineNumberInfo + ' - '+ msg;
		if (useClassDebugging){System.debug('ApexLoggingUtils.addLogMessage()[Private] - Created Log Message: "' + logMsg + '"');}
		system.debug(logLevel,logMsg);	// Add to debug log
		// If this was an error, 
		if (logLevel == LoggingLevel.Error && writeToExceptionLogOnError)
		{
			// -- Add a Corporate Log Record -- //
			//......
		}	
	}

	//=================================================================================
	// Method: getClassName(String traceLine) => String
	// Parses a stack trace string and returns the name of the Class that was executing.
	private static String getClassName(String traceLine)
	{
		String className;
		if (traceLine.startsWith('Class.'))
        	{className = traceLine.substringAfter('Class.').substringBefore(':').substringBeforeLast('.');}
        else
        	{className = traceLine.substringBefore(':').substringBeforeLast('.');}
        if (useClassDebugging){System.debug('ApexLoggingUtils.getClassName()[Private] - Retrieved class name: "' + className + '" from trace.');}
    	return className;
	}
	
	//=================================================================================
	// Method: getMethodName(String traceLine) => String
	// Parses a stack trace string and returns the name of the Method that was executing.
	private static String getMethodName(String traceLine)
	{
		String methodName = traceLine.substringBefore(':').substringAfterLast('.');
		if (useClassDebugging){System.debug('ApexLoggingUtils.getMethodName()[Private] - Retrieved method name: "' + methodName + '" from trace.');}
	    return methodName;
	}

	//=================================================================================
	// Method: getLineColNumberString (String traceLine) => String
	// Parses a stack trace string and returns the Line Number and Column Number where 
	// the error occurred, as a String.
	private static String getLineColNumberString(String traceLine)
	{
		String lineColNumberString = traceLine.substringAfter(':').trim();
		String lineNumString = lineColNumberString.substringBefore(','); // <= Ditch the column number - it's always 1.
		if (useClassDebugging){System.debug('ApexLoggingUtils.getLineColNumberString()[Private] - Retrieved Line Col/Num String: "' + lineNumString + '" from trace.');}
	    return lineNumString;
	}

}